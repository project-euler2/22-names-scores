names = sorted(open('p022_names.txt').read().replace('"', '').split(","))

def name_to_value(name):
    value = 0
    for letter in name:
        value += ord(letter.lower()) - 96
    return value

def names_scores(names_list):
    values = []
    for i in range(len(names_list)):
        values.append(name_to_value(names_list[i])*(i+1))
    return sum(values)
print(names_scores(names))